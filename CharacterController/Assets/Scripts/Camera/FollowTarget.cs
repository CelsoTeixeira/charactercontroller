using UnityEngine;

public abstract class FollowTarget : MonoBehaviour
{
    [SerializeField] public Transform target;
    public Transform Target { get { return this.target; } }

    [SerializeField] private bool autoTargetPlayer = true;

    protected Rigidbody _targetRigidbody;


    virtual protected void Start()
    {
        if (autoTargetPlayer)
        {
            FindTargetPlayer();
        }
    }

    void FixedUpdate()
    {
        
        if (autoTargetPlayer && (target == null || !target.gameObject.activeSelf))
        {
            FindTargetPlayer();            
        }

        if (target != null && (_targetRigidbody != null && !_targetRigidbody.isKinematic))
        {
            //Debug.Log("Follow");
            Follow(Time.deltaTime);
        }
    }

    protected abstract void Follow(float deltaTime);


    public void FindTargetPlayer()
    {
        if (target == null)
        {
            GameObject targetObject = GameObject.FindGameObjectWithTag("Player");

            if (targetObject)
            {
                SetTarget(targetObject.transform);
                _targetRigidbody = target.GetComponent<Rigidbody>();

            }
        }
    }

    public virtual void SetTarget(Transform newTransform)
    {
        target = newTransform;
    }

}